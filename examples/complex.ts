export { }

// Arrays

let list: number[] = [1, 2, 3]
// list.push('23') // Argument of type 'string' is not assignable to parameter of type 'number'

let list2: Array<number> = [1, 2, 3]
list2[3] = 123
// list2.push('23') // Argument of type 'string' is not assignable to parameter of type 'number'

// Tuples

// This is NOT and Array! This is a tuple!
let notAList: [number] = [1]
// notAList[1] = 123 // Type '123' is not assignable to type 'undefined'.

// Declare a tuple type
let x: [string, number];

// Initialize it
x = ["hello", 10]; // OK

// Initialize it incorrectly
// x = [10, "hello"]; // Error
// x = ["hello", 10, 23]; // Error
let t2: [string, number, boolean | undefined];
t2 = ['john', 123, true]
let arrayagain = t2.slice()
// arrayagain.push(123, 'est', true, 123)

// ===========

type option = "On" | 'Off'

// type Item = { name: string, value: number };
interface Item { name: string, value: number };

// const item: { name: string, value: number } = { name: 'item', value: 42 }
const item: Item = { name: 'item', value: 42 }

// function printItem(item: { name: string, value: number, x: 2 }) {
function printItem(item: Item) {
  return item.name + ' : ' + item.value
}

printItem(item)

// Or can be aliased for reuse

// indexed types

interface ArrayLike {
  [index: number]: string;
  length: number
}
const alike: ArrayLike = ['sad', 'asd', 'as']
// const alike: ArrayLike = ['sad', 213] // Type 'number' is not assignable to type 'string'.
interface Dictonary {
  [term: string]: string;
}

// enums

enum Role {
  USER = "user",
  ADMIN = "admin",
  // MODERATOR = "moderator",
};
function getPermissionsFor(
  role: Role,
): number | undefined {
  switch (role) {
    case Role.ADMIN:
      return 123;
    case Role.USER:
      return undefined;
    default:
      const _never: never = role; // exhaustiveness check
      throw _never
  }
}

enum AnotherEnum {
  SUBMITTED = 123,
  APPROVED, // 124
  PAID, // 125
  SOMETHING_ELSE = 1000 + 24, // expression
  MORE, // 1025
}

enum FileAccess {
  None = 1 << 0,    // 0001  
  Read = 1 << 1,    // 0010
  Write = 1 << 2,   // 0100
  Execute = 1 << 3, // 1000
  ReadWrite = FileAccess.Read | FileAccess.Write // 0110
}