/**
 * My adding func
 * @param {number} a
 * @param {number} b
 * @returns number
 */
export function add(a: number, b: number): number;
/**
 * @typedef {import('./add').Dessert} Dessert
 */
/**
 * @typedef Person
 * @property {string} name
 * @property {string} type
 *
 */
/** @type Person */
export const person: Person;
export type Dessert = import('./add').Dessert;
export type Person = {
    name: string;
    type: string;
};
