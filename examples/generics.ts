
const x: string[] = ['dasd']
const y: Array<string> = ['dasd']
const z: Array<string | number> = ['dasd']

const tictactoe: Array<Array<string>> = [
  ['X', 'O', 'X'],
  ['O', 'O', 'X'],
  ['X', 'O', 'X'],
]


type Ref<T> = {
  readonly current: T | null;
};
const ref1: Ref<number> = { current: 123 };
const ref2: Ref<string> = { current: "aaa" };

function getValue<T>(ref: Ref<T>): T {
  return ref.current;
}
getValue(ref1).toFixed()
getValue(ref2).toLocaleLowerCase()

function getValueAny<T>(ref: Ref<T>): any {
  return ref.current;
}

getValueAny(ref1).no.hints.no.checks


/// naive generics

declare function parse<T>(name: string): T
declare function serialize<T>(name: T): string

// parse<Array<string>>('123').push('213')
// parse('123') as Array<string>

const id = <T>(x: T): T => x;
const result = id<number>(1);


class Queue<T> {
  constructor(private data: T[] = []) { }
  push(item: T) { this.data.push(item); }
  pop(): T | undefined { return this.data.shift(); }
}
new Queue<string>(['string']).push('213')
new Queue<number>([123]).push(123)



function takeFirst<T>(arry: T[]): T {
  return arry[0]
}

const ar1: string = takeFirst<string>(['sda'])
const ar2: string = takeFirst(['sda']) // T = string because Array<string>
const ar3: string | number = takeFirst(['sda', 123]) // T = string | number
const ar4: number = takeFirst([1, 2, 3]) // takeFirst(arry: number[]): number


// function getLength(thing: string | Array<any>) {
// function getLength<T extends string | Array<any>>(thing: T) {
function getLength<T extends { length: number }>(thing: T) {
  return thing.length
}
// getLength(123)
getLength('123')

// extends - limit generics

type ObjWithName = { name: string };
function printName<T extends ObjWithName>(arg: T) { }
printName({ name: "Kate" }); // OK
printName({ name: "Michael", age: 22 }); // OK
// printName({ age: 22 }); // Error!


function makePair1(arg1: any, arg2: any) {
  return [arg1, arg2];
}
// makePair1(1,2).push() // push...items: any[]): 
function makePair2<T, U>(arg1: T, arg2: U) {
  return [arg1, arg2];
}
makePair2(1, '2')[0] // (string | number)

function makePair3<T, U>(arg1: T, arg2: U): [T, U] {
  return [arg1, arg2];
}
makePair3(1, '2')[0] // number

function merge<T, U>(x: T, y: U): T & U {
  return { ...x, ...y }
}
const obj: { x: number, y: number } = merge({ x: 1 }, { y: 2 })

function defaults<T, U extends T>(defaults: T, partial: U) {

}
const res = defaults({ 'a': 3, 'b': 2 }, { b: 3, c: 2, 'a': 3123 })
// const res = defaults({ 'a': 3, 'b': 2 }, { b: 3, c: 2, 'a': '4' }) // err


interface Point { x: number; y: number }
interface Vector { x: number; y: number, length: number }

let p: Point = { x: 1, y: 2 }
let v: Vector = { x: 1, y: 5, length: 324 }
p = v
// v = p // error

const defaultConfig = {
  port: 3000,
  host: "localhost",
};

type Config = typeof defaultConfig;
type Host = Config['host']