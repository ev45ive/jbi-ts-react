//@ts-check
// JSDoc 
// https://jsdoc.app/
/**
 * @typedef {import('./add').Dessert} Dessert
 */
/**
 * @typedef Person
 * @property {string} name
 * @property {string} type
 *
 */
/** @type Person */
export const person = { name: 'Alice', type: '' };
/**
 * My adding func
 * @param {number} a
 * @param {number} b
 * @returns number
 */
export function add(a, b) {
    return a + b;
}
//@ts-ignore
const x = add(1, '2');
const y = add(1, 2);
//@ts-expect-error
const z = add('2', {});
