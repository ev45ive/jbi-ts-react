
const x: boolean = true
const y: Boolean = new Boolean(1)
const z: boolean = Boolean(123)

const a: number = 123
const b: Number = new Number('1')
const c: number = Number('123')


// {}, Object, object type 
// https://stackoverflow.com/questions/49464634/difference-between-object-and-object-in-typescript


let someValue: unknown = "this is a string";
// someValue.length // Property 'length' does not exist on type 'unknown'
if ('length' in (someValue as any)) {
  let strLength: number = (someValue as string).length;
}

interface Tiger { name: 'T', pawns: 2, extra:'213' }
interface Animal { name: string, extra:123 }
let t: Tiger;
let an: Animal = t as unknown as Animal

const results: number[] = [1, 2, 3]
const casted: string[] = results as unknown as string[]
