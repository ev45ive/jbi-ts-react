# GIT
cd ..
git clone https://bitbucket.org/ev45ive/jbi-ts-react.git jbi-ts-react
cd jbi-ts-react
yarn 
yarn start

git stash -u 
git pull -f 


### Install
npm -v 
6.14.10

git --version 
git version 2.31.1.windows.1

code -v
1.40.1
8795a9889db74563ddd43eb0a897a2384129a619
x64

chrome

## Extensions
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## TSC


tsc --version
Version 4.4.4
npm i -g typescript

## Mac
https://medium.com/@ExplosionPills/dont-use-sudo-with-npm-still-66e609f5f92

npm config set prefix ~/.npm
npm i -g typescript

# TSC
tsc --module esnext --strict ./examples/add.ts 
tsc --module esnext --strict ./examples/add.ts  --watch 
tsc --module esnext --strict ./examples/add.ts  --init
./tsconfig.json
tsc
tsc --watch

# Ambient declaration
tsc -d --emitDeclarationOnly --allowJs index.js 
> index.d.ts

## React 

npx create-react-app ts-react-app --template typescript

### Playlists View

mkdir -p ./src/core/components/

mkdir -p ./src/playlists/containers/
mkdir -p ./src/playlists/components/

touch ./src/playlists/containers/PlaylistsView.tsx

touch ./src/playlists/components/PlaylistDetails.tsx
touch ./src/playlists/components/PlaylistForm.tsx
touch ./src/playlists/components/PlaylistList.tsx


## Music Search View
mkdir -p ./src/core/services/
touch ./src/core/services/MusicAPI.ts
touch ./src/core/services/Auth.ts
touch ./src/core/model/Search.ts

mkdir -p ./src/music/containers/
touch ./src/music/containers/MusicSearchView.tsx

mkdir -p ./src/music/components/
touch ./src/music/components/SearchForm.tsx
touch ./src/music/components/SearchResults.tsx
touch ./src/music/components/AlbumCard.tsx