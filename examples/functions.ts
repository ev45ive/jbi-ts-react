let myAdd1 = function (x: number, y: number): number {
  return x + y;
};

let myAdd2: (baseValue: number, increment: number) => number = function (x, y) {
  return x + y;
};

type AddFuncType2 = (baseValue: number, increment: number) => number;

interface AddFuncType {
  (baseValue: number, increment: number): number
}

// myAdd2() // myAdd2(baseValue: number, increment: number): number

let myAdd3: AddFuncType2 = function (x, y) {
  return x + y;
};

function ineedAdderFunc(cb: AddFuncType) {
  cb(1, 2)
}
ineedAdderFunc(myAdd1)
ineedAdderFunc(myAdd2)
ineedAdderFunc(myAdd3)

/// overloads

interface SearchFunc {
  (source: string, subString: string): boolean;
  /**
   * Check if string is in text starting from [startPos]
   */
  (source: string, subString: string, startPos: number): boolean; // overload
}
let mySearch: SearchFunc = (source: string, subString: string, startPos?: number) => {
  return source.includes(subString, startPos)
}
const someBool1 = mySearch('alice', 'ali')
const someBool2 = mySearch('alice', 'ice', 2)

// mySearch('alice','ice',2,2) // Expected 2-3 arguments, but got 4

function sum(initialNum: number, ...moreNumbers: number[]) { }


function handleButtonClick(this: GlobalEventHandlers | HTMLButtonElement) {
  if (this instanceof HTMLButtonElement) {
    this.type == 'submit'
  }
}
// handleButtonClick() // The 'this' context of type 'void' is not assignable to method's 'this' of type 'HTMLButtonElement'
const btn = document.createElement('button')
btn.onclick = handleButtonClick

/// Overloads
interface Character { name: string }

interface User extends Character { name: 'User' }
interface Admin extends Character { name: 'Admin' }
interface Moderator extends Character { name: 'Moderator' }

// declare function create1(name: string): Character // res as User
// declare function create2(name: string): Admin | User | Moderator // if(res.name === User){ ...

function create(name: "User"): User;
/**
 * Creates admin user
 * @param name Admin
 */
function create(name: "Admin"): Admin;
function create(name: "Moderator"): Moderator;
function create(name: string): Character {
  return { name }
}
const m = create("Moderator"); // m is a Moderator
m.name === 'Moderator'
// m.name === 'User' // This condition will always return 'false' since the types '"Moderator"' and '"User"' have no overlap
const u = create('User')

