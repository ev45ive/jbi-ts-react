import React from 'react'
import { Album, SimplifiedAlbum } from '../../core/model/Search'

interface Props {
  result: SimplifiedAlbum;
}

export const AlbumCard = ({ result }: Props) => {
  return (
    <div className="card">
      <img src={result.images[0].url} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{result.name}</h5>
      </div>
    </div>
  )
}
