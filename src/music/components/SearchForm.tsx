import React, { useEffect, useRef, useState } from 'react'

interface Props {
  onSearch: (query: string) => void;
}

export const SearchForm = ({ onSearch }: Props) => {
  const [query, setQuery] = useState('')

  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    inputRef.current?.focus()
  }, [])

  return (
    <div>
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Search"
          onChange={e => setQuery(e.target.value)}
          ref={inputRef} />

        <button className="btn btn-outline-secondary" type="button"
          onClick={() => onSearch(query)}>Search</button>
      </div>
    </div>
  )
}
