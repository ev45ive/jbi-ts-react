import { SimplifiedAlbum } from '../../core/model/Search';



export const mockAlbums: SimplifiedAlbum[] = [
  {
    id: '123', name: 'Album 123', type: 'album', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '234', name: 'Album 234', type: 'album', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '345', name: 'Album 345', type: 'album', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
  {
    id: '456', name: 'Album 456', type: 'album', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }
    ]
  },
];
