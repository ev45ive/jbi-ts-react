import React, { useEffect, useState } from 'react'
import { Album } from '../../core/model/Search'
import { musicService } from '../../core/services/MusicAPI'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import { mockAlbums } from './mockAlbums'


interface Props {

}

export const MusicSearchView = (props: Props) => {
  const [results, setResults] = useState<Album[]>([])
  const [query, setQuery] = useState('batman')
  const [message, setMessage] = useState('')

  const search = (query: string) => {
    setMessage('')
    setQuery(query);
    // setResults(mockAlbums as Album[])
  }

  useEffect(() => {
    musicService.search('album', query).then(res => {
      setResults(res)
    })
      .catch(err => setMessage(err.message))
  }, [query])

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}
          <SearchResults results={results} />
        </div>
      </div>
    </div>
  )
}
