interface Show {
  type: 'show';
  id: string;
  name: string;
  epispdes: number
}
interface Track {
  type: 'track';
  id: string;
  name: string;
  duration_ms: number
}

export interface Playlist {
  id: string;
  type: 'playlist';
  name: string;
  public: boolean;
  description: string;
  collaborative:boolean
  /**
   * List of tracks
   */
  tracks?: Track[]
}

const p = {} as Playlist;

if (p.tracks !== undefined) {
  const tracksno1 = p.tracks.length
}
const tracksno2 = p.tracks && p.tracks.length //  number | undefined
const tracksno3 = p.tracks?.length //  number | undefined
const tracksno4 = p.tracks ? p.tracks.length : 0 // number
const tracksno5 = p.tracks?.length || 0 // number
const tracksno6 = (p.tracks as Track[]).length  // number, or error in runtime
const tracksno7 = p.tracks!.length  // number, or error in runtime

/* ===================== */

function getId(id: number | string): string {
  if (typeof id === 'string') {
    return id
  }
  if (typeof id === 'number') {
    return id.toFixed(2)
  }
  throw new Error('Unexpected id ')
  // return id
}


type ResponseTypes = Playlist | Track // | Show

// showInfo({type:'playlist',description...})

function assertExhaustive(res: never): never {
  throw Error('Unexpected response')
}

function showInfo(res: ResponseTypes) {


  if ('duration_ms' in res) {
    return `${res.name} (${res.duration_ms} ms)`;
  }
  if ('public' in res) {
    return `${res.name} (${res.tracks?.length || 0} tracks)`;
  }
  assertExhaustive(res) // returns never -> 
  const wonthappen = 123 // Unreachable code detected.ts(7027
}

function showInfo2(res: ResponseTypes) {
  if (res.type === 'track') {
    return `${res.name} (${res.duration_ms} ms)`;
  }
  if (res.type === 'playlist') {
    return `${res.name} (${res.tracks?.length || 0} tracks)`;
  }
  assertExhaustive(res)
}


function showInfo3(res: ResponseTypes) {
  switch (res.type) {
    case 'track': return `${res.name} (${res.duration_ms} ms)`;
    case 'playlist': return `${res.name} (${res.tracks?.length || 0} tracks)`;
    default:
      assertExhaustive(res)
  }
}