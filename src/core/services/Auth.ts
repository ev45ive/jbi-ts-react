import { AuthProvider } from "./AuthProvider";


export class Auth implements AuthProvider {

  // private client_id:string,

  constructor(
    private client_id: string,
    private redirect_uri: string
  ) {
    // this.client_id = client_id
  }

  token: string = ''

  getToken(): string {
    return this.token
  }

  init() {
    const access_token = new URLSearchParams(window.location.hash).get('#access_token')
    if (access_token) {
      this.token = access_token
      sessionStorage.setItem('token', access_token)
    }

    this.token = sessionStorage.getItem('token') || this.token

    if (!this.token) {
      this.login()
    }

  }

  login() {

    var scope = 'user-read-private user-read-email';

    var url = 'https://accounts.spotify.com/authorize';
    url += '?response_type=token';
    url += '&client_id=' + encodeURIComponent(this.client_id);
    url += '&scope=' + encodeURIComponent(scope);
    url += '&redirect_uri=' + encodeURIComponent(this.redirect_uri);
    // url += '&state=' + encodeURIComponent(state);

    window.location.href = url

  }

  logout() {

  }
}

export const auth = new Auth(
  '2e2d6ebd28404f0a9b8e430e8a08045a',
  'http://localhost:3000/search'
)
