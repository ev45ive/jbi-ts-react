
export interface AuthProvider {
  getToken(): string;
}
