import { Album, AlbumsSearchResponse, Artist } from "../model/Search"
import axios from 'axios'
import { AuthProvider } from "./AuthProvider"
import { auth } from "./Auth"

type Tmap = {
  album: Album,
  artist: Artist,
}
export class MusicApi {

  constructor(private auth: AuthProvider) { }


  searchT<T extends keyof Tmap, K = Tmap[T]>(type: T, query: string): Promise<K[]> {
    return Promise.resolve<K[]>([])
  }

  async search(type: 'album', query: string): Promise<Album[]>
  async search(type: 'artist', query: string): Promise<Artist[]>
  async search(type: 'album' | 'artist', query: string): Promise<Album[] | Artist[]> {

    if (type === 'album') {
      try {

        const res = await axios.get<unknown>(`https://api.spotify.com/v1/search?type=${type}&q=${query}`, {
          headers: {
            Authorization: 'Bearer ' + this.auth.getToken()
          }
        })

        assertAlbumsRes(res.data)
        return res.data.albums.items

      } catch (err: unknown) {
        if (axios.isAxiosError(err)) {
          if (err.response) {
            throw new Error(err.response.data.error.message)
          }
        }
        throw new Error('Unexpeced error')

      }
      // return Promise.resolve<Album[]>([])
    }
    if (type === 'artist') {
      return Promise.resolve<Artist[]>([])
    }
    throw Error('unsuported type ' + type)
  }
}


export const musicService = new MusicApi(auth)

// musicService.search('album', '').then(res => { res })
// musicService.searchT('artist', '').then(res => { res })
// musicService.search('123', '').then(res => { res })


function validateAlbumsResp(res: any): res is AlbumsSearchResponse {
  return 'albums' in res && 'items' && res.albums && Array.isArray(res.albums.items)
}

function assertAlbumsRes(res: any): asserts res is AlbumsSearchResponse {
  if (!validateAlbumsResp(res)) {
    throw new Error('Unexpeced response')
  }
}