import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";

// type SaveFn = (draft: Playlist) => void

interface Props {
  playlist?: Playlist;
  // save: (draft: Playlist) => void
  // onSave: SaveFn
  onSave(draft: Playlist): void
  onCancel: () => void
}


const getEmptyPlaylist = (): Playlist => ({
  id: '',
  name: '',
  description: '',
  collaborative: false,
  public: false,
  type: 'playlist',
  tracks: []
});

export const PlaylistForm = ({ playlist = getEmptyPlaylist(), onSave, onCancel }: Props) => {

  const [playlistName, setPlaylistName] = useState(playlist.name)
  const [playlistPublic, setPlaylistPublic] = useState(playlist.public)
  const [playlistDescription, setPlaylistDescription] = useState(playlist.description)

  const updateNameHandler: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    setPlaylistName(event.currentTarget.value)
  };

  const save = () => {
    onSave({
      ...playlist,
      name: playlistName,
      public: playlistPublic,
      description: playlistDescription,
    })
  }

  return (
    <div>
      {/* <pancakes sauce="banana" /> */}

      {/* <pre>{JSON.stringify(playlist, null, 2)}</pre>
      <pre>{JSON.stringify({
        playlistName,
        playlistPublic,
        playlistDescription
      }, null, 2)}</pre> */}
      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Name</label>
        <input
          type="text"
          name="playlist_name"
          id="playlist_name"
          className="form-control"
          placeholder="Name"
          value={playlistName}
          onChange={updateNameHandler}
        />
        <small className="text-muted">{playlistName.length} / 170</small>
      </div>

      <div className="form-check mb-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            className="form-check-input"
            name="playlist_public"
            id="playlist_public"
            checked={playlistPublic}
            onChange={e => setPlaylistPublic(e.target.checked)}
          />
          Public
        </label>
      </div>

      <div className="form-group mb-3">
        <label htmlFor="playlist_description">Description:</label>
        <textarea
          className="form-control"
          name="playlist_description"
          id="playlist_description"
          rows={3}
          onChange={e => setPlaylistDescription(e.target.value)}
          value={playlistDescription}
        />
      </div>
      <button className="btn btn-danger" onClick={onCancel}>Cancel</button>
      <button className="btn btn-success" onClick={save}>Save</button>
    </div>
  );
};

{
  /* React.createElement('div',{ className:'group'},
  React.createElement('label',{ htmlFor:'group'}
) */
}
