import React from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist['id'];
  onSelect: (playlist_id: Playlist['id']) => void;
}

const cls = (...classes: (string | boolean)[]): string => classes.filter((c): c is string => typeof c === 'string').join(' ')

export const PlaylistList = ({ selectedId, playlists, onSelect }: Props) => {

  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) =>
          // <div className={"list-group-item " + (playlist.id === selectedId ? "active" : "")} key={playlist.id}>
          <div className={cls("list-group-item", playlist.id === selectedId && "active")} key={playlist.id}
            onClick={() => {
              onSelect(playlist.id)
            }}>
            {index + 1}. {playlist.name}
          </div>
        )}
      </div>
    </div>
  )
}
