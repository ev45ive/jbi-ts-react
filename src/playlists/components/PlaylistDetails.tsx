// tsrafc
import React from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlist: Playlist;
  onEdit(): void
}
export const PlaylistDetails = ({ playlist, onEdit }: Props) => {

  // if (playlist === undefined) {
  //   return <p>No playlist selected</p>
  // }

  return (
    <div>

      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>
        <dt>Public:</dt>

        {/* {playlist?.public ? <dd>Yes</dd> : <dd>No</dd>} */}
        <dd>{playlist.public ? 'Yes' : 'No'}</dd>

        <dt>Description</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>Edit</button>
    </div>
  )
}
