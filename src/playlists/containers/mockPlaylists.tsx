import { Playlist } from "../../core/model/Playlist";

// type Playlists2 = typeof mockPlaylists // extract type / infer
// type Playlist2 = (typeof mockPlaylists)[0] // extract type / infer

export const mockPlaylists: Playlist[] = [{
  id: '123',
  type: 'playlist',
  name: "Playlist 123",
  public: true,
  collaborative: false,
  description: 'playlist 123',
},
{
  id: '234',
  type: 'playlist',
  name: "Playlist 234",
  public: true,
  collaborative: false,
  description: 'playlist 234'
},
{
  id: '345',
  type: 'playlist',
  name: "Playlist 345",
  public: true,
  collaborative: false,
  description: 'playlist 345'
}];

const x = {
  id: '234',
  type: 'playlist' as const,
  name: "Playlist 234",
  public: true,
  description: 'playlist 234',
  extraData: 123
};
// extraData: 123 // polymorfism
// mockPlaylists.push(x)
// mockPlaylists[2].extraData // data hiding