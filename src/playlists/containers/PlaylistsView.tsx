import React, { useState } from 'react'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { Playlist } from "../../core/model/Playlist"
import { PlaylistForm } from '../components/PlaylistForm'
import { PlaylistList } from '../components/PlaylistList'
import { mockPlaylists } from './mockPlaylists'

interface Props {

}

type PlaylistViewMode = 'details' | 'edit' | 'create'


export const PlaylistsView = (props: Props) => {

  const [selected, setSelected] = useState<Playlist | undefined>()
  const [playlists, setPlaylists] = useState(mockPlaylists)

  const [mode, setMode] = useState<PlaylistViewMode>('details')

  const selectPlaylist = (id: string) => {
    setSelected(playlists.find(p => p.id === id));
  }

  const savePlaylist = (draft: Playlist): void => {
    setPlaylists(playlists => playlists.map(p => p.id === draft.id ? draft : p))
    setSelected(draft)
    setMode('details')
  }

  const newPlaylist = (draft: Playlist) => {
    draft.id = Number(Math.random() * 10_000).toString()
    
    // setPlaylists(nextState => [...nextState, draft])
    setPlaylists(playlists => [...playlists, draft])
    setSelected(draft)
    setMode('details')
  }
  const editMode = () => setMode('edit')
  const createMode = () => setMode('create')
  const cancel = () => setMode('details')

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            playlists={playlists}
            selectedId={selected?.id}
            onSelect={selectPlaylist}
          />
          <button className="btn-info btn float-end mt-3" onClick={createMode}>Create New</button>
        </div>
        <div className="col">
          {selected && <>
            {mode === 'details' && <PlaylistDetails playlist={selected} onEdit={editMode} />}
            {mode === 'edit' && <PlaylistForm playlist={selected} onCancel={cancel}
              onSave={savePlaylist} />}
          </>}

          {mode === 'create' && <PlaylistForm onCancel={cancel} onSave={newPlaylist} />}

          {!selected && mode !== 'create' && <p className="alert alert-info">No playlist selected</p>}
        </div>

      </div>
    </div>
  )
}
