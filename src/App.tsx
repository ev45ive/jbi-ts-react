import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { Route, Routes } from 'react-router';
import { MusicSearchView } from './music/containers/MusicSearchView';



function App() {
  return (
    <div>

      <div className="container">
        <div className="row">
          <div className="col">
            <h1>MusicApp</h1>
            <Routes>
              <Route path="/" element={<PlaylistsView />} />
              <Route path="/search" element={<MusicSearchView />} />
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
